const USERNAME = process.env.USERNAME
const HOST = process.env.HOST

const REPO = "git@gitlab.com:bukinator.b/node-ci.git"
const APP_PATH = `/home/${USERNAME}/node-ci-test`

module.exports = {
    apps: [
        {
            name: "NODE API TEST",
            script: "index.js",
            env_production: {
                NODE_ENV: "production"
            }
        }
    ],

    deploy: {
        production: {
            user: USERNAME,
            host: HOST,
            ref: "origin/master",
            repo: REPO,
            path: APP_PATH,
            "post-deploy":
                "npm install && pm2 reload ecosystem.config.js --env production"
        }
    }
}
